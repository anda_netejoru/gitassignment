package ro.ase.acs.csie.cts.as1.netejoruanda;

public interface Computable {
	public abstract void addVAT(double vatPercent);
}
