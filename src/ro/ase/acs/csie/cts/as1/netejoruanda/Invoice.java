package ro.ase.acs.csie.cts.as1.netejoruanda;

public class Invoice implements Computable{
	private double total;
	private String company;
	private double VATpercentage;
	
	public Invoice() {
		
	}
	
	public Invoice(double total, String company, double vatPercentage) {
		this.total = total;
		this.company = company;
		this.VATpercentage = vatPercentage;
	}

	@Override
	public void addVAT(double vatPercent) {
		this.total += vatPercent*total/100;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public double getVATpercentage() {
		return VATpercentage;
	}

	public void setVATpercentage(double vATpercentage) {
		VATpercentage = vATpercentage;
	}
	
	
}
