package ro.ase.acs.csie.cts.as1.netejoruanda;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git! The name of the license project is Financial document management system");
		
		Invoice invoice = new Invoice(100, "Telekom", 19);
		System.out.println("Initial sum: " + invoice.getTotal());
		invoice.addVAT(invoice.getVATpercentage());
		System.out.println("Total with VAT: " + invoice.getTotal());
	}

}
